FROM cyancor/ubuntu-cmake:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN git clone --branch boost-$BoostVersion https://github.com/boostorg/boost.git /boost && \
    chmod -R 0777 /boost && \
	cd /boost && \
	git submodule init && \
	git submodule update

RUN cd /boost && \
    ls -lah && \
    ./bootstrap.sh && \
	./b2